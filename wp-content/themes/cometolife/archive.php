<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cometolife
 */

get_header(); ?>

<?php
if ( have_posts() ) : ?>

	<section class="section section-archive">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<?php
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/loop', get_post_type() );
						endwhile;
						the_posts_navigation();
					?>
				</div>
			</div>
		</div>
	</section>

<?php
else :
	get_template_part( 'template-parts/content', 'none' );
endif; ?>

<?php
get_footer();
