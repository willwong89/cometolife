<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package cometolife
 */

get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main">

      <?php while ( have_posts() ) : the_post(); ?>
        <section class="section section-single-artist">

          <div class="container">
            <div class="row">
              <div class="col-md-8 col-md-push-2">
                <div class="post__container">


                  <h1 class="post__heading text-center"><?php the_title(); ?></h1>

                  <?php get_template_part('template-parts/post', 'share'); ?>

                  <?php
                    $youtube_embeded_code = get_field('youtube_embeded_code');
                    $content = get_field('content');
                    $bandcamp_embed_code = get_field('bandcamp_embed_code');
                    $bandcamp_embed_code_small = get_field('bandcamp_embed_code_small');
                  ?>

                  <?php if( $youtube_embeded_code ): ?>
                    <div class="embed-responsive embed-responsive-16by9">
                      <?php echo( $youtube_embeded_code ); ?>
                    </div>
                    <br />
                  <?php endif; ?>

                  <?php if( $bandcamp_embed_code_small ): ?>
                    <div class="visible-xs text-center">
                      <?php echo $bandcamp_embed_code_small; ?>
                    </div>
                  <?php endif; ?>

                  <div class="row">
                    <div class="<?php if( $bandcamp_embed_code ): ?>col-sm-8<?php else: ?>col-sm-12<?php endif; ?>">
                      <?php echo $content; ?>
                    </div>
                    <?php if( $bandcamp_embed_code ): ?>
                      <div class="col-sm-4 hidden-xs">
                        <?php echo $bandcamp_embed_code; ?>
                      </div>
                    <?php endif; ?>
                  </div>

                  <?php if( get_field('gallery') ): ?>
                    <?php get_template_part('template-parts/section-gallery'); ?>
                  <?php endif; ?>

                  <?php if( get_field('enable_featured_section') ): ?>
                    <?php get_template_part('template-parts/section-featured-slider'); ?>
                  <?php endif; ?>

                </div>
              </div>
            </div>
          </div>

        </section>
      <?php endwhile; ?>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
