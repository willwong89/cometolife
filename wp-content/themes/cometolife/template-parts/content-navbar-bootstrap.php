<?php
/**
 * Template part for Bootstrap navbar
 */

?>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header navbar-header--mobile visible-xs">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#header-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="//guayaki.com/" target="_blank">
        <?php
        /*
        <?php if ( get_header_image() ) : ?>
          <img alt="" src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>">
        <?php else: ?>
          <?php bloginfo('name'); ?>
        <?php endif; ?>
        */
        ?>
        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-simple.svg" alt="Guayaki Logo">
      </a>
    </div>

    <div id="header-navbar-collapse" class="collapse navbar-collapse">
      <?php
          wp_nav_menu( array(
              'menu'              => 'primary',
              'theme_location' => 'menu-1',
              'depth'             => 2,
              'container'         => 'div',
              // 'container_class'   => 'collapse navbar-collapse',
              // 'container_id'      => 'header-navbar-collapse',
              'menu_class'        => 'nav navbar-nav',
              'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
              'walker'            => new WP_Bootstrap_Navwalker())
          );
        ?>
      <div class="navbar-header navbar-header--desktop hidden-xs">
        <a class="navbar-brand" href="<?php echo home_url(); ?>">
          <img src="<?php echo get_template_directory_uri(); ?>/img/logo-simple.svg" alt="Guayaki Logo">
        </a>
      </div>
        <?php
          wp_nav_menu( array(
              'menu'              => 'secondary',
        			'theme_location' => 'menu-secondary',
              'depth'             => 2,
              'container'         => 'div',
              // 'container_class'   => 'collapse navbar-collapse',
              // 'container_id'      => 'header-navbar-collapse',
              'menu_class'        => 'nav navbar-nav navbar-nav-right',
              'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
              'walker'            => new WP_Bootstrap_Navwalker())
          );
      ?>
    </div>

  </div>
</nav>
