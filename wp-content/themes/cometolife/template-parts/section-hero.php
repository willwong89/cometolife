<?php
/**
 * Template part for displaying hero banner
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cometolife
 */

?>
<!-- section-hero -->
<?php
  $post_type = get_post_type();
  $post_id = get_the_ID();
  $page_handle = get_post_field( 'post_name', get_post() );

	if( is_home() ) {
	  $post_id = get_option( 'page_for_posts' );
  	$page_handle = 'blog';
	}

  if( is_shop() ) {
  	$post_id = get_option( 'woocommerce_shop_page_id' );
  	$page_handle = 'shop';
  }

  if( is_archive('artist') ) {
  	$post_id = 127;// artist page id
  }

  if( is_category() ) {
  	$term_id = get_queried_object()->term_id;
		$post_id = 'category_' . $term_id;
  }
?>

<?php if( $post_type == 'artist' ||
					$post_type == 'page' ||
					$post_type == 'post' ||
					is_shop() ||
					is_home() ): ?>

	<?php
		$hero_slider = get_field('hero_slider', $post_id);
		$hero_height = get_field('hero_height', $post_id);
		$hero_height_class = 'hero--full-height';
		if( $hero_height == 'Small' ) {
			$hero_height_class = 'hero--small';
		} else if( $hero_height == 'Tall' ) {
			$hero_height_class = 'hero--tall';
		}
	?>
	<?php if( $hero_slider ): ?>
		<?php
			$hero_slide_count = count($hero_slider);
			$parallax = ($hero_slide_count==1) ? true : false;
			$parallax = false;
		?>
		<section class="section-hero hero hero--<?php echo $page_handle ?> <?php echo $hero_height_class; ?>">

			<div class="<?php echo ($hero_slide_count>1) ? 'hero__slider' : 'hero__parallax'; ?>">
				<?php foreach( $hero_slider as $hero_slide ): ?>
					<?php
						$background_image = $hero_slide['background_image'];
						$heading = $hero_slide['heading'];
						$subheading = $hero_slide['subheading'];
						$link = $hero_slide['link'];
					?>
					<div class="<?php echo ($hero_slide_count>1) ? 'hero__slide' : 'hero__parallax__container'; ?>">

						<?php if( $background_image ): ?>
							<div class="hero__bg"
								 <?php if( $parallax ): ?>
								 	data-parallax="scroll" data-z-index="1" data-image-src="<?php echo $background_image['url']; ?>"
								 <?php else: ?>
								 style="background-image:url(<?php echo $background_image['url']; ?>);"
								 <?php endif; ?>></div>
						<?php endif; ?>

						<?php if( $link ): ?><a href="<?php echo $link; ?>" class="hero__link"><?php endif; ?>
						<div class="<?php echo ( $heading || $subheading ) ? 'hero__content text-center' : 'hero__content--blank'; ?>">
							<div class="container">
								<div class="hero__box">
									<?php if( $heading ): ?>
										<h1 class="hero__heading"><?php echo do_shortcode($heading); ?></h1>
									<?php endif; ?>
									<?php if( $subheading ): ?>
										<p class="hero__subheading"><?php echo do_shortcode($subheading); ?></p>
									<?php endif; ?>
								</div>
							</div>
						</div>
						<?php if( $link ): ?></a><?php endif; ?>

					</div>
				<?php endforeach; ?>
			</div>

			<?php if( $hero_slide_count > 1 ): ?>
			<script>
			$('.hero__slider').slick({
	  		autoplay: true,
				autoplaySpeed: 3000,
	  		slidesToScroll: 1,
				adaptiveHeight: true,
	  		fade: true,
	  		dots: true,
			  nextArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" ><i class="fa fa-chevron-right"></i></button>',
			  prevArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button"><i class="fa fa-chevron-left"></i></button>'
			});
			</script>
			<?php endif; ?>

		</section>

	<?php endif; ?>

<?php endif; ?>