<?php
  $featured_section_title = get_sub_field('featured_section_title');
  $featured_slider = get_sub_field('featured_slider');
  $slide_to_show = 4;

  if( is_singular('artist') ) {
    $enable_featured_section = get_field('enable_featured_section');
    $slide_to_show = 3;

    if( $enable_featured_section ) {
      $featured_section_title = get_field('featured_section_title');
      $featured_slider = get_field('featured_slider');
    } else {
      $featured_slider = false;
    }
  }
?>

<?php if( $featured_slider ): ?>
<section class="section section-featured-slider">

  <?php if( $featured_section_title ): ?>
    <h2 class="h4 text-center featured__title"><?php echo $featured_section_title; ?></h2>
  <?php endif; ?>

  <div class="featured__slider<?php if( is_front_page() ): ?> featured__slider--front-page<?php endif; ?>"
       data-slide-to-show="<?php echo $slide_to_show; ?>">
    <?php foreach( $featured_slider as $featured_slide ): ?>
      <?php
        $image = $featured_slide['image'];
        $text = $featured_slide['text'];
        $link = $featured_slide['link'];
      ?>
      <article class="featured__item">
        <div class="featured__item__body">

          <?php if( $link ): ?><a href="<?php echo $link; ?>" class="featured__item__link"><?php endif; ?>

          <div
           class="featured__item__image"
           style="background-image:url(&quot;<?php echo $image['sizes']['shop_single']; ?>&quot;"
           alt="<?php echo $image['alt']; ?>">
          </div>

          <?php if( $text ): ?>
          <div class="featured__item__content">
            <h5 class="featured__item__text"><?php echo $text; ?></h5>
          </div>
          <?php endif; ?>

          <?php if( $link ): ?></a><?php endif; ?>

        </div>
      </article>

    <?php endforeach; ?>
  </div>

    <script>
      var $featuredSlider = $('.featured__slider'),
          slidesToShow = $featuredSlider.data('slide-to-show');

      $featuredSlider.slick({
        autoplay: false,
        infinite: true,
        slidesToShow: slidesToShow,
        slidesToScroll: slidesToShow,
        nextArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" ><i class="fa fa-chevron-right"></i></button>',
        prevArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button"><i class="fa fa-chevron-left"></i></button>',
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          }
        ]
      });
    </script>

</section>
<?php endif; ?>
