<div class="post__share">
  <!-- AddToAny BEGIN -->
  <ul class="a2a_kit a2a_kit_size_32 a2a_default_style">
    <li>
      <a class="a2a_button_facebook a2a_counter"></a>
    </li>
    <li>
      <a class="a2a_button_twitter a2a_counter"></a>
    </li>
    <li>
      <a class="a2a_button_google_plus a2a_counter"></a>
    </li>
    <li>
      <a class="a2a_button_pinterest a2a_counter"></a>
    </li>
    <li>
      <a class="a2a_button_email a2a_counter"></a>
    </li>
  </ul>
  <script async src="https://static.addtoany.com/menu/page.js"></script>
  <!-- AddToAny END -->
</div>
