<?php
  $featured_section_title = get_sub_field('featured_section_title');
  $featured_blocks = get_sub_field('featured_blocks');

  $featured_arg = array(
    'from' => isset($_GET['block_from']) ? $_GET['block_from'] : 0,
    'to' => isset($_GET['block_to']) ? $_GET['block_to'] : false,
    'blocks_per_page' => 8
  );
  $featured_arg['to'] = $featured_arg['to'] ? $featured_arg['to'] : ($featured_arg['from'] + $featured_arg['blocks_per_page'] - 1);
  $featured_arg['next_from'] = $featured_arg['to'] + 1;
  if( $featured_arg['next_from'] > (count($featured_blocks)-1) ) {
    $featured_arg['next_from'] = false;
  }
?>

<?php if( $featured_blocks ): ?>
<section class="section section-featured-blocks">

  <?php if( $featured_section_title ): ?>
    <h2 class="h4 text-center featured__title"><?php echo $featured_section_title; ?></h2>
  <?php endif; ?>

  <?php
    // alter the $featured_blocks before display the blocks
    foreach( $featured_blocks as $i=>$featured_block ) {
      if( $i < $featured_arg['from'] || $i > $featured_arg['to'] ) {
        unset( $featured_blocks[$i] );
      }
    }
    $featured_blocks = array_values($featured_blocks);
  ?>

  <div class="featured__blocks">
    <div class="row row--condensed">
      <?php foreach( $featured_blocks as $i=>$featured_block ): ?>
        <?php
          $image = $featured_block['image'];
          $heading = $featured_block['heading'];
          $subheading = $featured_block['subheading'];
          $link = $featured_block['link'];
          $col = ( $i%8 == 0 || $i%8 == 1 ) ? 'col-xs-12 col-sm-6' : 'col-xs-12 col-sm-4';
        ?>
        <div class="<?php echo $col; ?>">
          <article class="featured__block">

            <?php if( $link ): ?><a target="_blank" href="<?php echo $link; ?>" class="featured__block__link"><?php endif; ?>

            <img class="featured__block__image" src="<?php echo $image['sizes']['shop_single']; ?>" alt="<?php echo $image['alt']; ?>">

            <?php if( $heading || $subheading ): ?>
            <div class="featured__block__content">
              <?php if( $heading ): ?>
                <h5 class="featured__block__heading"><?php echo $heading; ?></h5>
              <?php endif; ?>
              <?php if( $subheading ): ?>
                <p class="featured__block__subheading visible-xs visible-sm"><?php echo wp_trim_words( $subheading, 15, '...' ); ?></p>
                <p class="featured__block__subheading hidden-xs hidden-sm"><?php echo $subheading; ?></p>
              <?php endif; ?>
            </div>
            <?php endif; ?>

            <?php if( $link ): ?></a><?php endif; ?>

          </article>
        </div>
      <?php endforeach; ?>

      <?php if( $featured_arg['next_from'] ): ?>
        <div class="col-sm-12 featured__show-more">
          <a href="<?php echo add_query_arg( 'block_from', $featured_arg['next_from'], get_the_permalink() ); ?>" class="btn btn--black btn--show-more" style="display:block">Show More</a>
        </div>
      <?php endif; ?>

    </div>
  </div>

  <script>
    (function(){

      $('body').on('click', '.btn--show-more', function(e) {
        e.preventDefault();
        $(this).addClass('disabled').css('opacity', 0);

        var nextPageUrl = $(this).attr('href');
        loadPage( nextPageUrl );
      });

      var loadPage = function( nextPageUrl ) {

        if( nextPageUrl == undefined ) {
          return;
        }

        $.ajax({
          url: nextPageUrl,
          success: function(d){

            var $html = $(d),
                $featuredBlocks = $html.find('.featured__blocks');

            $('.featured__show-more').slideUp(function(){
              $(this).remove();
            });

            $featuredBlocks.find('.featured__block').css({
              marginTop: 100,
              opacity: 0
            });
            $featuredBlocks.find(' > .row').appendTo('.featured__blocks');

            setTimeout(function(){
              $('.featured__block').each(function(i){
                $(this).css({
                  marginTop: 0,
                  opacity: 1
                });
              });
            }, 300);

          }
        });

      };

      $(window).scroll(function(){

        var $showMore = $('.featured__show-more'),
            $showMoreBtn = $showMore.find('.btn--show-more'),
            nextPageUrl = $showMoreBtn.attr('href');

        if( !$showMore.length || $showMoreBtn.hasClass('disabled')|| nextPageUrl == undefined ) {
          return;
        }

        var scrollTop = $(window).scrollTop(),
            windowHeight = $(window).outerHeight(),
            showMoreTop = $showMore.position().top;

        if( scrollTop+(windowHeight/2) > showMoreTop ) {
          $showMoreBtn.click();
        }

      });

    }());
  </script>

</section>
<?php endif; ?>
