<?php
/**
 * Template part for the flexilbe content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cometolife
 */

?>
<div class="flexible-content">
	<?php
	  if( have_rows('flexible_content') ){
	    while( have_rows('flexible_content') ) {
	      the_row();
	      get_template_part( 'template-parts/'.get_row_layout() );
	    }
	  }
	?>
</div>
