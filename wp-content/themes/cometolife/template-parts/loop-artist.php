<?php
	$thumbnail_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
	$youtube_embeded_code = get_field('youtube_embeded_code', get_the_ID() );
?>
<article class="post-item">

	<header class="post-item__header<?php if( $youtube_embeded_code ): ?> post-item__header--video<?php endif; ?>">
		<a href="<?php the_permalink(); ?>">
			<?php if( $thumbnail_url ): ?>
				<img src="<?php echo $thumbnail_url ?>" alt="<?php echo get_the_title(); ?> thumbnail">
			<?php else: ?>
				<img src="<?php echo get_template_directory_uri(); ?>/img/post-item-placeholder.jpg" alt="Thumbnail placeholder">
			<?php endif; ?>
		</a>
	</header><!-- .entry-header -->

	<div class="post-item__content">
		<a href="<?php the_permalink(); ?>">
			<h3 class="post-item__title"><?php the_title(); ?></h3>
		</a>
		<div class="post-item__excerpt"><?php the_excerpt(); ?>
		</div>
		<a href="<?php the_permalink(); ?>" class="post-item__btn btn btn--black">Read Full Article</a>
	</div>

</article>

</article>
