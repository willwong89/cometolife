<?php
	$gallery = get_sub_field('gallery');


  if( is_singular('artist') ) {
    $gallery = get_field('gallery');
  }
?>
<section class="section section-gallery">

  <?php if( $gallery ): ?>
    <div class="gallery">
      <div class="gallery__row row">
        <?php foreach( $gallery as $i=>$gallery_item ): ?>
          <?php
            $col_class = ( $i%9 == 0 || $i%9 == 1 || $i%9 == 2 ) ? 'col-sm-6' : 'col-sm-4';
            if( $i%9 == 3 ) {
              $col_class = 'col-sm-8';
            }
            if( $i%9 == 0 ) {
              $col_class .= ' gallery__block--tall';
            }
          ?>
          <div class="gallery__block gallery__block--<?php echo $i%9; ?> <?php echo $col_class; ?>">
            <a href="<?php echo $gallery_item['url']; ?>" class="gallery__block__link" data-lightbox="Gallery">
              <span class="gallery__block__bg" style="background-image:url(<?php echo $gallery_item['sizes']['large']; ?>)" title="<?php echo $gallery_item['alt']; ?>"></span>
                <img class="gallery__block__img" src="<?php echo $gallery_item['sizes']['large']; ?>" alt="<?php echo $gallery_item['alt']; ?>" />
            </a>
          </div>
        <?php endforeach; ?>
      </div>

      <?php
      /*
      <div class="masonry__grid row">
        <div class="masonry__grid-sizer"></div>
        <?php foreach( $gallery as $i=>$gallery_item ): ?>
          <?php
            $masonry_item_class = 'col-sm-4';
            if( $i%5 == 0 && $i != 0 ) {
              $masonry_item_class = 'col-sm-8';
            }
          ?>
          <div class="masonry__grid-item col-xs-6 <?php echo $masonry_item_class; ?>">
            <img src="<?php echo $gallery_item['sizes']['large']; ?>" alt="<?php echo $gallery_item['alt']; ?>" />
          </div>
        <?php endforeach; ?>
      </div>
      */
      ?>
    </div>

  <?php else: ?>
    <p class="text-center">Empty gallery.</p>
  <?php endif; ?>

</section>
