<article class="post-item">
	<?php
		$content = get_the_content();
		$has_video = false;
		if (strpos($content, 'https://player.vimeo.com/video/') !== false ||
			  strpos($content, 'https://www.youtube.com/embed/') !== false) {
			$has_video = true;
		}
    $external_link = get_field('external_link');
	?>
	<header class="post-item__header<?php if( $has_video ): ?> post-item__header--video<?php endif; ?>">
    <?php if( $external_link ): ?>
      <a href="<?php echo $external_link; ?>" target="_blank">
    <?php else: ?>
      <a href="<?php the_permalink(); ?>">
    <?php endif; ?>
			<?php
        $thumbnail_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
        $image_url = get_template_directory_uri() . '/img/post-item-placeholder.jpg';
        if( $thumbnail_url ) {
          $image_url = get_the_post_thumbnail_url( get_the_ID(), 'large' );
        }
			?>
      <div class="post-item__image" style="background-image:url(&quot;<?php echo $image_url; ?>&quot;);"></div>
		</a>
	</header><!-- .entry-header -->

	<div class="post-item__content">
    <?php if( $external_link ): ?>
      <a href="<?php echo $external_link; ?>" target="_blank">
    <?php else: ?>
      <a href="<?php the_permalink(); ?>">
    <?php endif; ?>
			<h4 class="post-item__title"><?php the_title(); ?></h4>
		</a>

    <?php if( !is_front_page() ): ?>
      <div class="post-item__excerpt">
        <?php the_excerpt(); ?>
      </div>
    <?php endif; ?>

    <?php if( $external_link ): ?>
      <a href="<?php echo $external_link; ?>" target="_blank" class="post-item__btn btn btn--black hide">Read More</a>
    <?php else: ?>
      <a href="<?php the_permalink(); ?>" class="post-item__btn btn btn--black hide">Read Full Article</a>
    <?php endif; ?>
	</div>

</article>
