<?php
  $sidebar_blocks = get_sub_field('sidebar');
?>

<section class="section section-front-page">
  <div class="row row--condensed">
    <?php if( $sidebar_blocks ): ?>
      <?php
        $paged = isset($_GET['page_num']) ? $_GET['page_num'] : 1;
        $sidebar_blocks_count = count($sidebar_blocks);
        // $sidebar_block_index = mt_rand(0, $sidebar_blocks_count-1);
        $sidebar_block_index = ($paged-1)%$sidebar_blocks_count;
        $sidebar_block = $sidebar_blocks[$sidebar_block_index];

        ob_start();

        switch ( $sidebar_block['acf_fc_layout'] ) {
          case 'block-featured':

            $image = $sidebar_block['image'];
            if( $image ) {
              $image = wp_get_attachment_image_url( $image, '1200x' );
            }
            $content = $sidebar_block['content'];
            $link = $sidebar_block['link'];
          ?>

            <div class="sidebar__block sidebar__block--featured">
              <?php if( $link ): ?><a target="_blank" href="<?php echo $link; ?>" class="sidebar__block__link"><?php endif; ?>

              <div class="sidebar__block__body">

                <?php if( $image ): ?>
                  <div class="sidebar__block__bg" style="background-image:url(<?php echo $image; ?>);"></div>
                <?php endif; ?>

                <?php if( $content ): ?>
                  <div class="sidebar__block__content">
                    <?php echo $content; ?>
                  </div>
                <?php endif; ?>

              </div>

              <?php if( $link ): ?></a><?php endif; ?>

            </div>

          <?php
            break;

          case 'block-social-media':
            ?>

            <div class="sidebar__block sidebar__block--social-media">
              <div class="sidebar__block__body">
                <div class="sidebar__block__content">
                  <h4>Follow us:</h4>
                  <?php get_template_part('template-parts/list-social-media'); ?>
                </div>
              </div>
            </div>

          <?php
            break;
        }

        $sidebar_block_html = ob_get_contents();
        ob_end_clean();
      ?>
    <?php endif; ?>


    <div class="col-sm-12">
      <?php
        global $post;

        $paged = isset($_GET['page_num']) ? $_GET['page_num'] : 1;
        $posts_per_page = 6;
        $has_next_page = true;

        $post_count = wp_count_posts( 'post' );
        if( ($paged*$posts_per_page) >= $post_count->publish ) {
          $has_next_page = false;
        }

        $args = array(
          'post_type' => 'post',
          'posts_per_page' => $posts_per_page,
          'paged' => $paged
        );

        $hidden_tag = get_term_by('name', 'hidden', 'post_tag');
        if( $hidden_tag ) {
          $args['tag__not_in'] = array( $hidden_tag->term_id );
        }

        $blog_posts = get_posts( $args );
      ?>

      <div class="latest-posts">
        <div class="row row--condensed row--blog-posts">

          <?php $numItems = count($blog_posts); ?>
          <?php foreach( $blog_posts as $i => $post ) : setup_postdata($post); ?>
            <?php if( $sidebar_blocks && $i === 2 ): ?>
              <div class="col-sm-12">
                <div class="row row--condensed row--with-ads">
                  <div class="col-sm-8<?php if( $paged%2 == 0 ): ?> col-sm-push-4<?php endif; ?>">
                    <div class="row row--condensed">
            <?php endif; ?>

            <div class="col-sm-6">
              <?php
                get_template_part( 'template-parts/loop', get_post_type() );
              ?>
            </div>

            <?php if( $sidebar_blocks && $i === ($numItems-1) ): ?>
                    </div>
                  </div>
                  <div class="col-sm-4<?php if( $paged%2 == 0 ): ?> col-sm-pull-8<?php endif; ?>">
                    <div class="row row--condensed row--ads">
                      <?php echo $sidebar_block_html ?>
                    </div>
                  </div>
                </div>
              </div>
            <?php endif; ?>

          <?php endforeach; wp_reset_postdata(); ?>

          <?php if( $has_next_page ): ?>
            <div class="col-xs-12 latest-posts__show-more">
              <a href="<?php echo add_query_arg( 'page_num', $paged+1, get_the_permalink() ); ?>"
                 class="btn btn--secondary btn--show-more"
                 style="display:block;">See All</a>
            </div>
          <?php endif; ?>
        </div>
      </div>


      <script type="text/javascript">
        (function(){
          var normaize = function() {
            $('.post-item .post-item__title').normalizeHeight({
              by_row: true
            });
            $('.post-item .post-item__excerpt').normalizeHeight({
              by_row: true
            });
          };

          $(window).load(function() {
            normaize();
          });

          $(window).resize(function() {
            normaize();
          });

          $('body').on('click', '.btn--show-more', function(e) {

            e.preventDefault();
            $(this).addClass('disabled').css('opacity', 0);

            var nextPageUrl = $(this).attr('href');
            loadPage( nextPageUrl );
          });

          var loadPage = function( nextPageUrl ) {

            if( nextPageUrl == undefined ) {
              return;
            }

            $.ajax({
              url: nextPageUrl,
              success: function(d){

                var $html = $(d),
                    $latestPosts = $html.find('.latest-posts');

                $('.latest-posts__show-more').slideUp(function(){
                  $(this).remove();
                });

                $latestPosts.find('.post-item').css({
                  marginTop: 100,
                  opacity: 0
                });
                $latestPosts.find(' > .row').appendTo('.latest-posts');

                setTimeout(function(){
                  $('.latest-posts .post-item').each(function(i){
                    $(this).css({
                      marginTop: 0,
                      opacity: 1
                    });
                  });
                  normaize();
                }, 300);

                setTimeout(function(){
                  normaize();
                }, 500);

                loadingPage = false;

              }
            });

          };

          var loadCount = 0;
          $(window).scroll(function() {
            var $btnShowMore = $('.btn--show-more');
            if( $btnShowMore.hasClass('disabled') ) {
              return;
            }

            var scrollTop = $(window).scrollTop()
                $frontPageSection = $('.section-front-page'),
                bottomBoundary = $frontPageSection.offset().top + $frontPageSection.outerHeight() - $(window).outerHeight() - (offset=50);

            if( scrollTop > bottomBoundary && loadCount <4 ) {
              loadCount++;
              $btnShowMore.click();
            }
          });

        }());
      </script>

    </div>

  </div>
</section>
