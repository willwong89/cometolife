<?php
  $content = get_sub_field('content');
?>
<section class="section section-content">

	<?php echo $content; ?>

</section>
