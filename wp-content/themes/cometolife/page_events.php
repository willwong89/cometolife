<?php
/**
* Template Name: Events
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cometolife
 */

get_header(); ?>
<!-- page.php -->
  <div id="primary" class="content-area">
    <main id="main" class="site-main">

      <?php
        // WooCommerce Stuff
        $page_id = get_the_ID();
        $is_woocommerce = false;
        if( $page_id == get_option( 'woocommerce_shop_page_id' ) ) {
          $is_woocommerce = true;
          the_content();
        }

        if( $page_id == get_option( 'woocommerce_cart_page_id' ) ||
            $page_id == get_option( 'woocommerce_checkout_page_id' ) ||
            $page_id == get_option( 'woocommerce_pay_page_id' ) ||
            $page_id == get_option( 'woocommerce_thanks_page_id' ) ||
            $page_id == get_option( 'woocommerce_myaccount_page_id' ) ||
            $page_id == get_option( 'woocommerce_edit_address_page_id' ) ||
            $page_id == get_option( 'woocommerce_view_order_page_id' ) ||
            $page_id == get_option( 'woocommerce_terms_page_id' ) ) {
            $is_woocommerce = true;
          ?>

          <div class="container">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <?php the_content(); ?>
              </div>
            </div>
          </div>

          <?php
        }
      ?>

      <?php if( !$is_woocommerce ): ?>
        <?php
          $col = 'col-md-10 col-md-push-1';
        ?>
        <div class="container">

          <div class="events-wrap">
            <?php
              $upcoming_events = get_field('upcoming_events');
              $past_events = get_field('past_events');
              $event_categories = [];
              $event_categories['Upcoming Events'] = $upcoming_events;
              $event_categories['Past Events'] = $past_events;
              $event_categories['NoEvent'] ;
            ?>

            <div class="row">
              <div class="col-xs-12">
                <div class="events-toggle">
                  <ul class="events-toggle__list text-center">
                    <?php $index = 0; ?>
                    <?php foreach( $event_categories as $key => $events ): ?>
                      <?php if( $events ): ?>
                        <li
                         class="events-toggle__item<?php if($index==0): ?> active<?php endif; ?>"
                         event-toggle-item>
                          <a
                           href="#"
                           class="events-toggle__link"
                           data-target="<?php echo $key; ?>"
                           event-toggle>
                            <?php echo $key; ?></a>
                        </li>
                        <?php $index += 1; ?>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </ul>
                </div>
              </div>
            </div>

            <?php $index = 0; ?>
            <?php foreach( $event_categories as $key => $events ): ?>
              <?php if( $events ): ?>
                <div
                 <?php if($index!=0): ?>style="display:none;"<?php endif; ?>
                 data-events-name="<?php echo $key; ?>"
                 events>
                  <?php if( $events ): ?>
                    <?php foreach( $events as $event ): ?>
                      <?php
                        $image = $event['image'];
                        $image_url = ( $image['sizes']['large'] ) ? $image['sizes']['large'] : false;
                        $heading = $event['heading'];
                        $content = $event['content'];
                        $button_text = $event['button_text'];
                        $button_link = ( $event['button_link'] ) ? $event['button_link'] : '#';
                      ?>

                      <article class="post-item">
                        <header class="post-item__header">
                          <a href="<?php echo $button_link; ?>">
                            <div class="post-item__image"<?php if( $image_url ): ?> style="background-image:url(&quot;<?php echo $image_url; ?>&quot;);"<?php endif; ?>></div>
                          </a>
                        </header><!-- .entry-header -->

                        <div class="post-item__content">
                          <?php if( $heading ): ?>
                            <a href="<?php echo $button_link; ?>">
                              <h4 class="post-item__title"><?php echo $heading; ?></h4>
                            </a>
                          <?php endif; ?>

                        <?php if( $content ): ?>
                          <div class="post-item__excerpt">
                            <p><?php echo $content; ?></p>
                          </div>
                        <?php endif; ?>

                        <?php if( $button_text ): ?>
                          <a href="<?php echo $button_link; ?>" target="_blank" class="post-item__btn btn btn--black"><?php echo $button_text; ?></a>
                        <?php endif; ?>
                        </div>
                      </article>

                    <?php endforeach; ?>
                  <?php endif; ?>
                </div>
                <?php $index += 1; ?>
              <?php endif; ?>
            <?php endforeach; ?>

            <script>
              (function(){
                var selectors = {
                  toggle: '[event-toggle]',
                  toggleItem: '[event-toggle-item]',
                  events: '[events]'
                };

                $(selectors.toggle).click(function(e){
                  e.preventDefault();
                  var target = $(this).attr('data-target');
                  $(selectors.toggleItem).removeClass('active');
                  $(this).closest(selectors.toggleItem).addClass('active');
                  $(selectors.events).hide();
                  $(selectors.events).filter('[data-events-name="' + target + '"]').show();
                });
              }());
            </script>
          </div>

          <div class="row">
            <div class="<?php echo $col; ?>">
              <?php
                while ( have_posts() ) : the_post();

                  // get_template_part( 'template-parts/content', 'page' );
                  get_template_part( 'template-parts/content', 'flexible' );

                  // If comments are open or we have at least one comment, load up the comment template.
                  if ( comments_open() || get_comments_number() ) :
                    comments_template();
                  endif;

                endwhile; // End of the loop.
              ?>
            </div>
          </div>
        </div>
      <?php endif; ?>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
