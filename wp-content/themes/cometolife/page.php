<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cometolife
 */

get_header(); ?>
<!-- page.php -->
  <div id="primary" class="content-area">
    <main id="main" class="site-main">

      <?php
        // WooCommerce Stuff
        $page_id = get_the_ID();
        $is_woocommerce = false;
        if( $page_id == get_option( 'woocommerce_shop_page_id' ) ) {
          $is_woocommerce = true;
          the_content();
        }

        if( $page_id == get_option( 'woocommerce_cart_page_id' ) ||
            $page_id == get_option( 'woocommerce_checkout_page_id' ) ||
            $page_id == get_option( 'woocommerce_pay_page_id' ) ||
            $page_id == get_option( 'woocommerce_thanks_page_id' ) ||
            $page_id == get_option( 'woocommerce_myaccount_page_id' ) ||
            $page_id == get_option( 'woocommerce_edit_address_page_id' ) ||
            $page_id == get_option( 'woocommerce_view_order_page_id' ) ||
            $page_id == get_option( 'woocommerce_terms_page_id' ) ) {
            $is_woocommerce = true;
          ?>

          <div class="container">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <?php the_content(); ?>
              </div>
            </div>
          </div>

          <?php
        }
      ?>

      <?php if( !$is_woocommerce ): ?>
        <?php
          $col = 'col-md-10 col-md-push-1';
        ?>
        <div class="container">
          <div class="row">
            <div class="<?php echo $col; ?>">
              <?php
                while ( have_posts() ) : the_post();

                  // get_template_part( 'template-parts/content', 'page' );
                  get_template_part( 'template-parts/content', 'flexible' );

                  // If comments are open or we have at least one comment, load up the comment template.
                  if ( comments_open() || get_comments_number() ) :
                    comments_template();
                  endif;

                endwhile; // End of the loop.
              ?>
            </div>
          </div>
        </div>
      <?php endif; ?>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
