<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cometolife
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,400i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=IM+Fell+Great+Primer" rel="stylesheet">
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>

    <?php
      $slider_is_bg = false;
      if( is_front_page() || is_singular('post') || is_singular('artist') ) {
        $slider_is_bg = true;
      }
    ?>

    <?php
      if( $slider_is_bg ) {
        ?>
        <div class="section-background">
          <?php get_template_part('template-parts/section-hero'); ?>
        </div>
        <?php
      }
    ?>

    <div id="page" class="site">

    	<header id="masthead" class="site-header" role="banner">
    		<?php
    			get_template_part('template-parts/content-navbar', 'bootstrap');
    		?>
        <?php
          if( !$slider_is_bg ) {
            get_template_part('template-parts/section-hero');
          }
        ?>
    	</header><!-- #masthead -->

      <div id="content" class="site-content">

