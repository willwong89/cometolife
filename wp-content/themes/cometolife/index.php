<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cometolife
 */

get_header(); ?>

<?php
if ( have_posts() ) : ?>

	<section class="section-archive">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<?php
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/loop' );
						endwhile;
						the_posts_navigation();
					?>
				</div>
			</div>
		</div>
	</section>

<?php
else :
	get_template_part( 'template-parts/content', 'none' );
endif; ?>

<?php
get_sidebar();
get_footer();
