<?php

  include 'acf/functions.php';
  include 'shortcodes/functions.php';

  function pprint_r($arg) {
  	echo '<pre>';
  	print_r($arg);
  	echo '</pre>';
  }

  function cometolife_init_custom_post_types() {

    // add artist post type
    $labels = array(
      'name'               => _x( 'Artists', 'post type general name', 'custom' ),
      'singular_name'      => _x( 'Artist', 'post type singular name', 'custom' ),
      'menu_name'          => _x( 'Artists', 'admin menu', 'custom' ),
      'name_admin_bar'     => _x( 'Artist', 'add new on admin bar', 'custom' ),
      'add_new'            => _x( 'Add New', 'testimonial', 'custom' ),
      'add_new_item'       => __( 'Add New Artist', 'custom' ),
      'new_item'           => __( 'New Artist', 'custom' ),
      'edit_item'          => __( 'Edit Artist', 'custom' ),
      'view_item'          => __( 'View Artist', 'custom' ),
      'all_items'          => __( 'All Artists', 'custom' ),
      'search_items'       => __( 'Search Artists', 'custom' ),
      'parent_item_colon'  => __( 'Parent Artist:', 'custom' ),
      'not_found'          => __( 'No artists found.', 'custom' ),
      'not_found_in_trash' => __( 'No artists found in Trash.', 'custom' )
    );

    $args = array(
      'labels' => $labels,
      'supports' => array(
        'title',
        // 'editor',
        'thumbnail',
        'excerpt'
      ),
      'public' => true,
      'has_archive' => true,
      'menu_icon' => 'dashicons-art',
      'menu_position' => 50,
    );

    register_post_type( 'artist', $args );
    add_post_type_support( 'artist', 'page-attributes' );

    /*
    $labels = array(
      'name'              => _x( 'Property Categories', 'taxonomy general name' ),
      'singular_name'     => _x( 'Property Category', 'taxonomy singular name' ),
      'search_items'      => __( 'Search Property Categories' ),
      'all_items'         => __( 'All Property Categories' ),
      'parent_item'       => __( 'Parent Property Category' ),
      'parent_item_colon' => __( 'Parent Property Category:' ),
      'edit_item'         => __( 'Edit Property Category' ),
      'update_item'       => __( 'Update Property Category' ),
      'add_new_item'      => __( 'Add New Property Category' ),
      'new_item_name'     => __( 'New Property Category Name' ),
      'menu_name'         => __( 'Property Category' ),
    );

    register_taxonomy(
      'property_category',
      'property',
      array(
        'labels' => $labels,
        'hierarchical' => true,
      )
    );
    */

  }
  add_action( 'init', 'cometolife_init_custom_post_types' );


  function cometolife_post_save_post( $post_id ) {

    if( get_post_type($post_id) == 'post' || get_post_type($post_id) == 'page' ) {

      $flexible_content = get_field('flexible_content', $post_id);

      if( $flexible_content ) {

        $searchable_content = '';

        foreach( $flexible_content as $content ) {
          if( $content['acf_fc_layout'] == 'section-content') {
            $searchable_content .= $content['content'];
          }
        }

        // Update this post
        $my_post = array(
          'ID'           => $post_id,
          'post_content' => $searchable_content,
        );

        // Update the post into the database
        wp_update_post( $my_post );
      }
    }
  }
  add_action('acf/save_post', 'cometolife_post_save_post', 20); // run after ACF saves the $_POST['acf'] data

  function cometolife_artist_save_post( $post_id ) {

    if( get_post_type($post_id) == 'artist' ) {

      $content = get_field('content');

      $my_post = array(
        'ID'           => $post_id,
        'post_content' => $content,
      );

      wp_update_post( $my_post );
    }
  }
  add_action('acf/save_post', 'cometolife_artist_save_post', 20);

  function cometolife_add_image_size() {
    add_image_size( '1200x', 1200 ); // 1200 pixels wide (and unlimited height)
    add_image_size( '2000x', 2000 ); // 2000 pixels wide (and unlimited height)
  }
  add_action( 'after_setup_theme', 'cometolife_add_image_size' );


  function cometolife_register_nav_menu() {
    register_nav_menu('menu-secondary', __( 'Secondary Menu' ));
    register_nav_menu('social-media', __( 'Social Media' ));
  }
  add_action( 'init', 'cometolife_register_nav_menu' );


  // Callback function to filter the MCE settings
  function cometolife_mce_before_init_insert_formats( $init_array ) {

    // Define the style_formats array
    $style_formats = array(
      // Each array child is a format with it's own settings
      array(
        'title' => 'Button Link - White',
        'selector' => 'a',
        'classes' => 'btn btn-white',
      ),
      array(
        'title' => 'Button Link - Blue',
        'selector' => 'a',
        'classes' => 'btn btn-blue',
      ),
      array(
        'title' => 'Button Link - Orange',
        'selector' => 'a',
        'classes' => 'btn btn-orange',
      ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;
  }
  add_filter( 'tiny_mce_before_init', 'cometolife_mce_before_init_insert_formats' );


  // Exclude all posts tagged with "Hidden"
  function cometolife_exclude_post_hidden_tag($query) {

    $hidden_tag = get_term_by('name', 'hidden', 'post_tag');

    if( $hidden_tag && !is_admin() ) {
      set_query_var('tag__not_in', array( $hidden_tag->term_id ));
    }
  }
  add_action('pre_get_posts', 'cometolife_exclude_post_hidden_tag');


  function remove_menus(){

    // remove_menu_page( 'index.php' );                  //Dashboard
    // remove_menu_page( 'jetpack' );                    //Jetpack*
    // remove_menu_page( 'edit.php' );                   //Posts
    // remove_menu_page( 'upload.php' );                 //Media
    // remove_menu_page( 'edit.php?post_type=page' );    //Pages
    remove_menu_page( 'edit-comments.php' );          //Comments
    // remove_menu_page( 'themes.php' );                 //Appearance
    // remove_menu_page( 'plugins.php' );                //Plugins
    // remove_menu_page( 'users.php' );                  //Users
    // remove_menu_page( 'tools.php' );                  //Tools
    // remove_menu_page( 'options-general.php' );        //Settings

  }
  add_action( 'admin_menu', 'remove_menus' );

