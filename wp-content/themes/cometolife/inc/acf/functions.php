<?php

  if( class_exists('acf') ) {

    include 'custom-admin-columns.php';

    if( function_exists('acf_add_options_page') ) {
      include 'theme-settings.php';
    }

    function my_acf_google_map_api( $api ){
      $api['key'] = 'AIzaSyCigqLF8q7eEM1y2a_uHT6k0rlxPTVN6Bc';
      return $api;
    }
    add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

  }
