<?php
  /*
  acf_add_options_page(array(
    'page_title'  => 'Theme Options',
    'menu_title'  => 'Theme Options',
    'menu_slug'   => 'theme-options',
    'capability'  => 'edit_posts',
    'redirect'    => false,
    'icon_url' => 'dashicons-format-status',
  ));
  */

  $acf_section_options = acf_add_options_page(array(
    'page_title'  => 'Theme Options',
    'menu_title'  => 'Theme Options',
    'menu_slug'   => 'theme-options',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));

  // add sub page
  // acf_add_options_sub_page(array(
  //   'page_title'  => 'Footer Section',
  //   'menu_title'  => 'Footer Section',
  //   'menu_slug'   => 'footer-section',
  //   'parent_slug'   => $acf_section_options['menu_slug'],
  // ));

