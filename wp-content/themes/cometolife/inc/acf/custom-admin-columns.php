<?php
  // http://www.elliotcondon.com/advanced-custom-fields-admin-custom-columns/

  /*-------------------------------------------------------------------------------
    Custom THEPOSTTYPE Columns
  -------------------------------------------------------------------------------*/
  function the_THEPOSTTYPE_columns( $columns ) {
    $columns = array(
      'cb'    => '<input type="checkbox" />',
      'title'   => 'Title',
      'categories' => 'Categories',
      'address' => 'Address',
    );
    return $columns;
  }
  add_action("manage_THEPOSTTYPE_posts_custom_column", "the_custom_THEPOSTTYPE_columns");

  function the_custom_THEPOSTTYPE_columns( $column ) {

    if( $column == 'id' ) {

      echo get_the_ID();

    } elseif( $column == 'address') {

      print_r( get_field('address') );

    }
  }
  add_filter("manage_edit-THEPOSTTYPE_columns", "the_THEPOSTTYPE_columns");

  function the_THEPOSTTYPE_column_register_sortable( $columns ) {
    $columns['id'] = 'id';

    return $columns;
  }
  add_filter("manage_edit-THEPOSTTYPE_sortable_columns", "the_THEPOSTTYPE_column_register_sortable" );

