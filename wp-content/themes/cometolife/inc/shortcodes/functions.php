<?php

  //[form-email-sign-up]
  function form_email_sign_up_func() {
    ob_start();
    get_template_part('template-parts/form-email-sign-up');
    $html = ob_get_contents();
    ob_end_clean();
    return $html;
  }
  add_shortcode( 'form-email-sign-up', 'form_email_sign_up_func' );

  //[list-social-media]
  function list_social_media_func() {
    ob_start();
    get_template_part('template-parts/list-social-media');
    $html = ob_get_contents();
    ob_end_clean();
    return $html;
  }
  add_shortcode( 'list-social-media', 'list_social_media_func' );
