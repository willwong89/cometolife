<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cometolife
 */

?>

  <?php if( is_front_page() ): ?>
    <section class="section section--instafeed">

      <div class="container-fluid">

        <div id="Instafeed"
             class="instafeed row row--condensed"
             data-count="6"
             data-access-token="5505120955.b534787.b1f83c5ece2a404ba6145cd20fa61483"
             data-grid="col-xs-4 col-sm-2">

        </div>

      </div>

      <script type="text/javascript">
        (function(){

          var instafeedEl = $('#Instafeed'),
              instafeedId = 'Instafeed';

          var userFeed = new Instafeed({
            get: 'user',
            userId: 'self',
            target: instafeedId,
            accessToken: instafeedEl.attr('data-access-token'),
            sortBy: 'most-recent',
            resolution: 'standard_resolution',
            limit: instafeedEl.attr('data-count'),
            template: '<div class="'+instafeedEl.attr('data-grid')+' instafeed__item"><div class="instafeed__item__body"><div class="instafeed__item__bg" style="background-image: url({{image}});"></div><a class="instafeed__item__link" href="{{link}}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></div></div>'
          });

          userFeed.run();

        }());
      </script>

    </section>
  <?php endif; ?>

  </div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
      <div class="row text-center">
        <div class="col-sm-12">
          <a href="//guayaki.com/" target="_blank" class="wreath-home-link">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Guayaki Logo">
          </a>
        </div>
        <div class="col-sm-12">
          <?php get_template_part('template-parts/list-social-media'); ?>
        </div>
      </div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
