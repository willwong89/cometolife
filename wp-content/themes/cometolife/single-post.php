<?php
/**
 * The template for displaying all artist posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package cometolife
 */

get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main">

      <section class="section section-single-post">
        <div class="container">
          <div class="row">
            <div class="col-md-8 col-md-push-2">

              <div class="post__container">

                <section class="section section-post-header">

                  <h1 class="post__heading text-center">
                    <?php
                      echo get_field('heading') ? get_field('heading') : get_the_title();
                      echo get_field('subheading') ? '<br /><small class="post__subheading">' . get_field('subheading') . '</small>' : '';
                    ?>
                  </h1>

                  <?php get_template_part('template-parts/post', 'share'); ?>

                </section>

                <?php
                  while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', 'flexible' );

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) :
                      /*
                      ?>
                    <section class="section-single">
                      <div class="container">
                        <div class="row">
                          <div class="col-md-6 col-md-offset-3">
                            <?php comments_template(); ?>
                          </div>
                        </div>
                      </div>
                    </section>
                      <?php
                      */
                    endif;

                  endwhile; // End of the loop.
                ?>

                <section class="section section-post-share">
                  <?php get_template_part('template-parts/post', 'share'); ?>
                </section>

                <section class="post__related section section-featured-slider">

                  <h2 class="h4 text-center featured__title">Related Articles</h2>

                  <?php
                    $related_posts = get_posts(
                      array(
                        'category__in' => wp_get_post_categories( $post->ID ),
                        'numberposts'  => 5,
                        'post__not_in' => array( $post->ID )
                      )
                    );
                  ?>
                  <?php if( $related_posts ): ?>
                    <div class="featured__slider featured__slider--post">
                      <?php foreach( $related_posts as $post ): setup_postdata($post);?>
                        <?php
                          $image_url = get_the_post_thumbnail_url();
                          $text = get_the_title();
                          $link = get_the_permalink();
                        ?>
                        <article class="featured__item">
                          <div class="featured__item__body">

                            <?php if( $link ): ?><a href="<?php echo $link; ?>" class="featured__item__link"><?php endif; ?>

                            <div class="featured__item__bg" style="background-image:url(<?php echo $image_url; ?>);"></div>

                            <?php if( $text ): ?>
                            <div class="featured__item__content">
                              <h5 class="featured__item__text"><?php echo $text; ?></h5>
                            </div>
                            <?php endif; ?>

                            <?php if( $link ): ?></a><?php endif; ?>

                          </div>
                        </article>

                      <?php endforeach; wp_reset_postdata(); ?>
                    </div>
                    <script>
                      $('.featured__slider').slick({
                        autoplay: false,
                        infinite: true,
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        nextArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" ><i class="fa fa-chevron-right"></i></button>',
                        prevArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button"><i class="fa fa-chevron-left"></i></button>',
                        responsive: [
                          {
                            breakpoint: 768,
                            settings: {
                              slidesToShow: 2,
                              slidesToScroll: 2
                            }
                          }
                        ]
                      });
                    </script>
                  <?php endif;  ?>

                </section>

              </div>

            </div>
          </div>
        </div>
      </section>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
