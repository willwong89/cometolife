wordpress-blank
===============
##Gulp
### Install Gulp Globally
```sh
$ npm install --global gulp
```
### Install Gulp Packages
```sh
$ cd wordpress-blank
$ npm install
```

----------

##Composer
### Install Composer Globally (on Local Machine)
```sh
$ curl -sS https://getcomposer.org/installer | php
$ mv composer.phar /usr/local/bin/composer
```
### Install Composer Globally (on Remote Server)
```sh
$ curl -sS https://getcomposer.org/installer | php
```

### Install WordPress core files & plugins with Composer (on Local Machine)
```sh
$ cd wordpress-blank
$ composer install
```

### Install WordPress core files & plugins with Composer (on Remote Server)
```sh
$ cd wordpress-blank
$ php composer.phar install
```

##Reference(s)
 - https://deliciousbrains.com/using-composer-manage-wordpress-themes-plugins/
 - http://wpackagist.org/